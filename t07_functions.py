from cpymad.madx import Madx
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.pyplot import cm
from pybt.tools.plotters import *
from matplotlib.patches import Ellipse, Rectangle
import requests
import sys
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from ezdxf import recover
from ezdxf.addons.drawing import RenderContext, Frontend
from ezdxf.addons.drawing.matplotlib import MatplotlibBackend
from madxtools.plot_tool import *

# Function to rotate a point around another point
def rotate_around_point(x, y, cx, cy, angle_degrees):
    # Translate point to origin
    x -= cx
    y -= cy

    # Convert angle to radians
    angle_radians = np.radians(angle_degrees)

    # Perform rotation
    x_rotated = x * np.cos(angle_radians) - y * np.sin(angle_radians)
    y_rotated = x * np.sin(angle_radians) + y * np.cos(angle_radians)

    # Translate back
    x_rotated += cx
    y_rotated += cy

    return x_rotated, y_rotated

def calc_absolute_deviation(my_dataframe):
    output_dataframe = pd.DataFrame(columns=["name","x", "y", "x_dev_absolute","y_dev_absolute"])
    
    for i in range(len(my_dataframe)):
        if (my_dataframe.X_S[i]-my_dataframe.X_E[i])>=0:
            sign = +1
        else:
            sign = -1

        if type(my_dataframe.NAME[i])==str:
            angle = (my_dataframe.Y_S[i]-my_dataframe.Y_E[i])/(my_dataframe.X_S[i]-my_dataframe.X_E[i])

            x_e_dev = my_dataframe.X_E[i]+sign*my_dataframe.DEV_LONG_VALUE_E[i]*np.cos(angle)-sign*my_dataframe.DEV_RAD_VALUE_E[i]*np.sin(angle)
            y_e_dev = my_dataframe.Y_E[i]+sign*my_dataframe.DEV_LONG_VALUE_E[i]*np.sin(angle)+sign*my_dataframe.DEV_RAD_VALUE_E[i]*np.cos(angle)

            x_s_dev = my_dataframe.X_S[i]+sign*my_dataframe.DEV_LONG_VALUE_S[i]*np.cos(angle)-sign*my_dataframe.DEV_RAD_VALUE_S[i]*np.sin(angle)
            y_s_dev = my_dataframe.Y_S[i]+sign*my_dataframe.DEV_LONG_VALUE_S[i]*np.sin(angle)+sign*my_dataframe.DEV_RAD_VALUE_S[i]*np.cos(angle)

            new_rows = pd.DataFrame([
                {"name": str(my_dataframe.NAME[i])+".E" , "x": my_dataframe.X_E[i], "y": my_dataframe.Y_E[i], "x_dev_absolute": x_e_dev, "y_dev_absolute": y_e_dev},
                {"name": str(my_dataframe.NAME[i])+".S" , "x": my_dataframe.X_S[i], "y": my_dataframe.Y_S[i], "x_dev_absolute": x_s_dev, "y_dev_absolute": y_s_dev}
            ])
            
            output_dataframe = pd.concat([output_dataframe, new_rows], ignore_index=True)
    return output_dataframe

def f61d(p, exn, eyn, betx0, bety0, alfx0, alfy0, Dx0, Dy0, Dpx0, Dpy0):
    #################################### Initialize MADX ####################################
    with open('tempfile', 'w') as f:
        madx = Madx(stdout=f,stderr=f)
        madx.option(verbose=True, debug=False, echo=True, warn=True, twiss_print=False)

    madx.input(requests.get("https://gitlab.cern.ch/acc-models/acc-models-tls/-/raw/2021/ps_extraction/f61d/f61d.dbx").text)
    madx.input(requests.get("https://gitlab.cern.ch/acc-models/acc-models-tls/-/raw/2021/ps_extraction/f61d/f61d.ele").text)
    madx.input(requests.get("https://gitlab.cern.ch/acc-models/acc-models-tls/-/raw/2021/ps_extraction/f61d/f61d.seq").text)
    madx.input(requests.get("https://gitlab.cern.ch/acc-models/acc-models-tls/-/raw/2021/ps_extraction/f61d/f61d_proton.str").text)

    madx.command.beam(particle='PROTON',pc=p,exn=exn,eyn=eyn)
    madx.input('BRHO      := BEAM->PC * 3.3356;')

    madx.input('SELECT, FLAG=TWISS, COLUMN=NAME,KEYWORD,S,L,K1L,BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APERTYPE,APER_1,APER_2,APER_3,APER_4,KMIN,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;')
    # madx.input('SELECT, FLAG=aperture, COLUMN=apertype;')

    madx.input("kQFN1 = "+str(0.4797)+";")
    madx.input("kQDN2 = "+str(-0.173)+";")
    madx.input("kQFN3 = "+str(0.1986)+";")

    madx.use(sequence="f61d")
    twiss_f61d = madx.twiss(betx=betx0, bety=bety0, alfx=alfx0, alfy=alfy0, Dx=Dx0, Dy=Dy0, Dpx=Dpx0, Dpy=Dpy0).dframe()
    survey_f61d = madx.survey()

    return survey_f61d


def t8(p, exn, eyn, betx0, bety0, alfx0, alfy0, Dx0, Dy0, Dpx0, Dpy0):
    #################################### Initialize MADX ####################################
    with open('tempfile', 'w') as f:
        madx = Madx(stdout=f,stderr=f)
        madx.option(verbose=True, debug=False, echo=True, warn=True, twiss_print=False)

    madx.input(requests.get("https://gitlab.cern.ch/eljohnso/acc-models-tls-eliott-fork/-/raw/EliottBranch/ps_extraction/f61t8/f61t8_op.seq?ref_type=heads").text)
    madx.input(requests.get("https://gitlab.cern.ch/eljohnso/acc-models-tls-eliott-fork/-/raw/EliottBranch/ps_extraction/f61t8/f61t8_op.str?ref_type=heads").text)

    madx.command.beam(particle='PROTON',pc="24",exn=exn,eyn=eyn)
    madx.input('BRHO      := BEAM->PC * 3.3356;')

    madx.input('SELECT, FLAG=TWISS, COLUMN=NAME,KEYWORD,S,L,K1L,BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APERTYPE,APER_1,APER_2,APER_3,APER_4,KMIN,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;')

    madx.input("kQFN1 = "+str(0.4797)+";")
    madx.input("kQDN2 = "+str(-0.173)+";")
    madx.input("kQFN3 = "+str(0.1986)+";")

    madx.use(sequence="f61t8_op")
    twiss_f61t8 = madx.twiss(betx=betx0, bety=bety0, alfx=alfx0, alfy=alfy0, Dx=Dx0, Dy=Dy0, Dpx=Dpx0, Dpy=Dpy0).dframe()
    survey_f61t8 = madx.survey()

    return survey_f61t8

def t07(p, exn, eyn, betx0, bety0, alfx0, alfy0, Dx0, Dy0, Dpx0, Dpy0): 
    #################################### Initialize MADX ####################################
    with open('tempfile', 'w') as f:
        madx = Madx(stdout=f,stderr=f)
        madx.option(verbose=True, debug=False, echo=True, warn=True, twiss_print=False)

    madx.call("madx_files/f61t07.seq")
    madx.call("madx_files/f61t07.str")

    madx.command.beam(particle='PROTON',pc=p,exn=exn,eyn=eyn)
    madx.input('BRHO      := BEAM->PC * 3.3356;')

    madx.input('SELECT, FLAG=TWISS, COLUMN=NAME,KEYWORD,S,L,K1L,BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APERTYPE,APER_1,APER_2,APER_3,APER_4,KMIN,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;')

    madx.input("kQFN1 = "+str(0.4797)+";")
    madx.input("kQDN2 = "+str(-0.173)+";")
    madx.input("kQFN3 = "+str(0.1986)+";")

    madx.use(sequence="f61t07")
    twiss_f61t12 = madx.twiss(betx=betx0, bety=bety0, alfx=alfx0, alfy=alfy0, Dx=Dx0, Dy=Dy0, Dpx=Dpx0, Dpy=Dpy0).dframe()
    survey_f61t12 = madx.survey()
    return survey_f61t12

def translate_rotate(survey):
    x_change_ref = 75490
    y_change_ref = 9309.75

    f61_start_x = 2016.2080241761512
    f61_start_y = 2196.823487632785

    # Convert MAD-X survey to dataframe
    d = {'x': survey.z, 'y': survey.x, 'keyword': survey.keyword, 'l': survey.l}
    df_survey = pd.DataFrame(data=d)

    # Translate MAD-X survey
    d = {'x': df_survey.x + f61_start_x + x_change_ref, 'y': df_survey.y + f61_start_y + y_change_ref, 'keyword': survey.keyword, 'l': survey.l}
    df_survey_translated = pd.DataFrame(data=d)

    # Rotation parameters
    angle_degrees = -7.2
    center_of_rotation = (df_survey_translated.x[0], df_survey_translated.y[0])

    # Apply rotation to specific row
    for i in df_survey_translated.index:
        x_rotated, y_rotated = rotate_around_point(df_survey_translated.loc[i, 'x'], df_survey_translated.loc[i, 'y'], *center_of_rotation, angle_degrees)
        df_survey_translated.loc[i, 'x_rot'] = x_rotated
        df_survey_translated.loc[i, 'y_rot'] = y_rotated

    return df_survey_translated

def plot_magnets(df, x, y, ax):
    # Plotting the rectangle
    for index, row in df.iterrows():
        if row['keyword'] == "rbend":
            rectangle_width = 1.246/2
            rbend_index = index
            start_index = rbend_index - 1

            # Coordinates of the start and end of the rbend
            start_x, start_y = df.iloc[start_index][x], df.iloc[start_index][y]
            end_x, end_y = df.iloc[rbend_index][x], df.iloc[rbend_index][y]
            length = df.iloc[rbend_index]['l']

            # Create a rectangle
            rect = patches.Rectangle((start_x, start_y), length, rectangle_width, angle=np.degrees(np.arctan2(end_y - start_y, end_x - start_x)), edgecolor=None, facecolor='#8DC600', alpha=0.8, lw=2, zorder=99)
            ax.add_patch(rect)
            rect = patches.Rectangle((start_x, start_y), length, -rectangle_width, angle=np.degrees(np.arctan2(end_y - start_y, end_x - start_x)), edgecolor=None, facecolor='#8DC600', alpha=0.8, lw=2, zorder=99)
            ax.add_patch(rect)

        if row['keyword'] == "quadrupole":
            rectangle_width = 0.844/2
            rbend_index = index
            start_index = rbend_index - 1

            # Coordinates of the start and end of the rbend
            start_x, start_y = df.iloc[start_index][x], df.iloc[start_index][y]
            end_x, end_y = df.iloc[rbend_index][x], df.iloc[rbend_index][y]
            length = df.iloc[rbend_index]['l']

            # Create a rectangle
            rect = patches.Rectangle((start_x, start_y), length, rectangle_width, angle=np.degrees(np.arctan2(end_y - start_y, end_x - start_x)), edgecolor=None, facecolor='lightblue', alpha=0.8, lw=2, zorder=99)
            ax.add_patch(rect)
            rect = patches.Rectangle((start_x, start_y), length, -rectangle_width, angle=np.degrees(np.arctan2(end_y - start_y, end_x - start_x)), edgecolor=None, facecolor='lightblue', alpha=0.8, lw=2, zorder=99)
            ax.add_patch(rect)

        if row['keyword'] == "octupole":
            rectangle_width = 0.844/2
            rbend_index = index
            start_index = rbend_index - 1

            # Coordinates of the start and end of the rbend
            start_x, start_y = df.iloc[start_index][x], df.iloc[start_index][y]
            end_x, end_y = df.iloc[rbend_index][x], df.iloc[rbend_index][y]
            length = df.iloc[rbend_index]['l']

            # Create a rectangle
            rect = patches.Rectangle((start_x, start_y), length, rectangle_width, angle=np.degrees(np.arctan2(end_y - start_y, end_x - start_x)), edgecolor=None, facecolor='darkslateblue', alpha=0.8, lw=2, zorder=99)
            ax.add_patch(rect)
            rect = patches.Rectangle((start_x, start_y), length, -rectangle_width, angle=np.degrees(np.arctan2(end_y - start_y, end_x - start_x)), edgecolor=None, facecolor='darkslateblue', alpha=0.8, lw=2, zorder=99)
            ax.add_patch(rect)

        if row['keyword'] == "instrument":
            rectangle_width = 0.5/2
            rbend_index = index
            start_index = rbend_index - 1

            # Coordinates of the start and end of the rbend
            start_x, start_y = df.iloc[start_index][x], df.iloc[start_index][y]
            end_x, end_y = df.iloc[rbend_index][x], df.iloc[rbend_index][y]
            length = df.iloc[rbend_index]['l']

            # Create a rectangle
            rect = patches.Rectangle((start_x, start_y), length, rectangle_width, angle=np.degrees(np.arctan2(end_y - start_y, end_x - start_x)), edgecolor=None, facecolor='red', alpha=0.8, lw=2, zorder=99)
            ax.add_patch(rect)
            rect = patches.Rectangle((start_x, start_y), length, -rectangle_width, angle=np.degrees(np.arctan2(end_y - start_y, end_x - start_x)), edgecolor=None, facecolor='red', alpha=0.8, lw=2, zorder=99)
            ax.add_patch(rect)

        if row['keyword'] == "monitor":
            rectangle_width = 0.5/2
            rbend_index = index
            start_index = rbend_index - 1

            # Coordinates of the start and end of the rbend
            start_x, start_y = df.iloc[start_index][x], df.iloc[start_index][y]
            end_x, end_y = df.iloc[rbend_index][x], df.iloc[rbend_index][y]
            length = df.iloc[rbend_index]['l']

            # Create a rectangle
            rect = patches.Rectangle((start_x, start_y), length, rectangle_width, angle=np.degrees(np.arctan2(end_y - start_y, end_x - start_x)), edgecolor=None, facecolor='magenta', alpha=0.8, lw=2, zorder=99)
            ax.add_patch(rect)
            rect = patches.Rectangle((start_x, start_y), length, -rectangle_width, angle=np.degrees(np.arctan2(end_y - start_y, end_x - start_x)), edgecolor=None, facecolor='magenta', alpha=0.8, lw=2, zorder=99)
            ax.add_patch(rect)

        if (row['keyword'] == "hkicker") or (row['keyword'] == "vkicker"):
            rectangle_width = 0.730/2
            rbend_index = index
            start_index = rbend_index - 1

            # Coordinates of the start and end of the rbend
            start_x, start_y = df.iloc[start_index][x], df.iloc[start_index][y]
            end_x, end_y = df.iloc[rbend_index][x], df.iloc[rbend_index][y]
            length = df.iloc[rbend_index]['l']

            # Create a rectangle
            rect = patches.Rectangle((start_x, start_y), length, rectangle_width, angle=np.degrees(np.arctan2(end_y - start_y, end_x - start_x)), edgecolor=None, facecolor='orange', alpha=0.8, lw=2, zorder=99)
            ax.add_patch(rect)
            rect = patches.Rectangle((start_x, start_y), length, -rectangle_width, angle=np.degrees(np.arctan2(end_y - start_y, end_x - start_x)), edgecolor=None, facecolor='orange', alpha=0.8, lw=2, zorder=99)
            ax.add_patch(rect)
    return


def calc_initial_condition(E_cin_per_nucleon):
    # E_cin_per_nucleon = 2.0
    # Ion properties
    A = 208.0
    Z = 82.0
    N = 126.0
    charge = 54.0
    m_proton_GeV = 0.93828
    m_neutron_GeV = 0.93957
    m_electron_GeV = 0.000511
    m_u_GeV = 0.9315
    mass_defect_GeV = Z * m_proton_GeV + N * m_neutron_GeV + (Z - charge) * m_electron_GeV - A * m_u_GeV
    E_0 = Z * m_proton_GeV + N * m_neutron_GeV - mass_defect_GeV

    p = E_0 * np.sqrt((((E_cin_per_nucleon * A) / E_0) + 1) ** 2 - 1)

    gamma = p/charge/0.938
    beta = np.sqrt(1-gamma**(-2))

    print(p/charge)
    print(f"gamma = {round(gamma,3)}")
    print(f"beta = {round(beta,3)}")
    print(f"p = {round(p/charge,3)} GeV/c")

    Brho = 3.33564*p/charge

    # Beam characteristics
    exn = 4.92e-06
    eyn = 3.4e-06
    sige = 0.000412
    ex = exn/(beta*gamma)
    ey = eyn/(beta*gamma)

    # Initial conditions
    betx0 = 53.074
    bety0 = 3.675
    alfx0 = -13.191
    alfy0 = 0.859
    Dx0 = 0.13
    Dy0 = 0.0
    Dpx0 = 0.02
    Dpy0 = 0.0
    exn = 2.53e-05
    eyn =  6.94e-06
    sige = 0.00045

    return p, exn, eyn, betx0, bety0, alfx0, alfy0, Dx0, Dy0, Dpx0, Dpy0, ex, ey, sige

def plot_graph_with_dispersion(twiss, ex, sige, ey, fontsize=16, figsize=(10,6), height_ratios=[1,3,3,3,3]):
    
    fig, ax = plt.subplots(5,1, figsize=figsize, tight_layout=True, sharex=True, height_ratios=height_ratios)
    
    draw_synoptic(ax[0], twiss)
    
    ax[1].plot(twiss['s'], beam_size(twiss['betx'], twiss['dx'], ex, sige, 1)+twiss.x, alpha=1.0, color ="b", zorder=0)
    ax[1].plot(twiss['s'], -beam_size(twiss['betx'], twiss['dx'], ex, sige, 1)+twiss.x, alpha=1.0, color ="b", zorder=0)
    ax[1].set_xlim(0,twiss.s[-1])
    
    ax[2].plot(twiss['s'], beam_size(twiss['bety'], twiss['dy'], ey, sige, 1)+twiss.y, alpha=1.0, color ="r", zorder=0)
    ax[2].plot(twiss['s'], -beam_size(twiss['bety'], twiss['dy'], ey, sige, 1)+twiss.y, alpha=1.0, color ="r", zorder=0)
    ax[2].set_xlim(0,twiss.s[-1])
    
    ax[1].grid()
    ax[1].set_ylim(-0.12, 0.12)
    ax[1].set_ylabel(r"$\sigma_{H}$ [m]", fontsize=fontsize)
    
    ax[2].grid()
    ax[2].set_ylim(-0.12, 0.12)
    ax[2].set_ylabel(r"$\sigma_{V}$ [m]", fontsize=fontsize)
    
    ax[3].plot(twiss.s, twiss.betx, alpha=1.0, color ="darkblue", zorder=0, label=r"$\beta_{x}$")
    ax[3].plot(twiss.s, twiss.bety, alpha=1.0, color ="darkred", zorder=0, label=r"$\beta_{y}$")
    ax[3].set_xlim(0,twiss.s[-1])
    
    ax[3].legend(loc="upper left")
    ax[3].set_ylim(0, 200)
    ax[3].grid()
    ax[3].set_ylabel(r"$\beta$ [m]", fontsize=fontsize)
    
    ax[4].plot(twiss.s, twiss.dx, alpha=1.0, color ="darkblue", zorder=0, label=r"$D_{x}$")
    ax[4].plot(twiss.s, twiss.dy, alpha=1.0, color ="darkred", zorder=0, label=r"$D_{y}$")
    ax[4].set_xlim(0,twiss.s[-1])
    
    ax[4].legend(loc="upper left")
    # ax[4].set_ylim(0, 200)
    ax[4].grid()
    ax[4].set_ylabel("D [m]", fontsize=fontsize)

    draw_aperture_circle(ax[1], twiss, "aper_1")
    draw_aperture_circle(ax[2], twiss, "aper_1")
    draw_aperture_rectangle(ax[1], twiss, "aper_1")
    draw_aperture_rectangle(ax[2], twiss, "aper_2")
    draw_aperture_racetrackH(ax[1], twiss, "aper_1", "aper_2", "aper_3", "aper_4")
    draw_aperture_racetrackV(ax[2], twiss, "aper_1", "aper_2", "aper_3", "aper_4")

    plt.show()