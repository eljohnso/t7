#!/usr/bin/env python

import sys

import xobjects as xo
import xpart as xp
import xcoll as xc
import numpy as np
import xtrack as xt
import pickle

import matplotlib.pyplot as plt
from scipy.stats import *
from madxtools.particles import *
import pybobyqa

print("Hello")
print(sys.argv[0]) # Name of the script
print(sys.argv[1]) # First argument
print(sys.argv[2])

# Save arguments to a pickle file
with open(f'data.pkl', 'wb') as f:
    pickle.dump(sys.argv, f)