import xobjects as xo
import xpart as xp
import xcoll as xc
import numpy as np
import xtrack as xt
import pickle
from matplotlib.pyplot import cm
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from scipy.stats import *
from madxtools.particles import *
from madxtools.math import *
from cpymad.madx import Madx

from cpymad.madx import Madx
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.pyplot import cm
from pybt.tools.plotters import *
from matplotlib.patches import Ellipse, Rectangle
import sys
import matplotlib.pyplot as plt
import matplotlib.patches as patches

from madxtools.plot_tool import *

def calc_initial_condition(E_cin_per_nucleon):
    # E_cin_per_nucleon = 2.0
    # Ion properties
    A = 208.0
    Z = 82.0
    N = 126.0
    charge = 54.0
    m_proton_GeV = 0.93828
    m_neutron_GeV = 0.93957
    m_electron_GeV = 0.000511
    m_u_GeV = 0.9315
    mass_defect_GeV = Z * m_proton_GeV + N * m_neutron_GeV + (Z - charge) * m_electron_GeV - A * m_u_GeV
    E_0 = Z * m_proton_GeV + N * m_neutron_GeV - mass_defect_GeV

    p = E_0 * np.sqrt((((E_cin_per_nucleon * A) / E_0) + 1) ** 2 - 1)

    gamma = p/charge/0.938
    beta = np.sqrt(1-gamma**(-2))

    print(p/charge)
    print(f"gamma = {round(gamma,3)}")
    print(f"beta = {round(beta,3)}")
    print(f"p = {round(p/charge,3)} GeV/c")

    Brho = 3.33564*p/charge

    # Beam characteristics
    exn = 4.92e-06
    eyn = 3.4e-06
    sige = 0.000412
    ex = exn/(beta*gamma)
    ey = eyn/(beta*gamma)

    # Initial conditions
    betx0 = 53.074
    bety0 = 3.675
    alfx0 = -13.191
    alfy0 = 0.859
    Dx0 = 0.13
    Dy0 = 0.0
    Dpx0 = 0.02
    Dpy0 = 0.0
    exn = 2.53e-05
    eyn =  6.94e-06
    sige = 0.00045

    return p, exn, eyn, betx0, bety0, alfx0, alfy0, Dx0, Dy0, Dpx0, Dpy0, ex, ey, sige



from scipy.stats import pearsonr, spearmanr, kendalltau

initial_quad_strength = [-0.28611781,  0.48998671,  0.4186652 , -0.09344907, -0.07466559,
        0.15355204, 29.08274068, 60.83029156]

num_part = 2*5000
seed = np.random.randint(low=1, high=int(4e9), size=num_part, dtype=np.uint32)

with open('tempfile', 'w') as f:
    madx = Madx(stdout=f,stderr=f)
    madx.option(verbose=True, debug=False, echo=True, warn=True, twiss_print=False)

madx.call("simple_seq.seq")

p, exn, eyn, betx0, bety0, alfx0, alfy0, dx0, dy0, dpx0, dpy0, ex, ey, sige = calc_initial_condition(0.5)

madx.input(f'''
beam, particle=PROTON, pc={p};
use, sequence=simple_seq;
''')

madx.input('SEQEDIT, sequence=simple_seq;')
madx.input(f'MOVE, ELEMENT = OCT1, TO={initial_quad_strength[6]};')
madx.input('MOVE, ELEMENT = Q1, TO=1, FROM=OCT1;')
madx.input(f'MOVE, ELEMENT = OCT2, TO={initial_quad_strength[7]};')
madx.input('MOVE, ELEMENT = Q2, TO=1, FROM=OCT2;')
madx.input('ENDEDIT;')
madx.use(sequence="simple_seq")
madx.input('SELECT, FLAG=TWISS, COLUMN=NAME,KEYWORD,S,L,K1L,BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APERTYPE,APER_1,APER_2,KMIN,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;')
twiss = madx.twiss(betx=betx0, bety=bety0, alfx=alfx0, alfy=alfy0, Dx=dx0, Dy=dy0, Dpx=dpx0, Dpy=dpy0).dframe()

line = xt.Line.from_madx_sequence(
    sequence=madx.sequence.simple_seq,
    allow_thick=True,
    enable_align_errors=True,
    deferred_expressions=True,
)
line.particle_ref = xt.Particles(mass0=xt.PROTON_MASS_EV,
                            gamma0=madx.sequence.simple_seq.beam.gamma)

tw_init = xt.TwissInit(betx=betx0, bety=bety0, alfx=alfx0, alfy=alfy0, dx=dx0, dy=dy0, dpx=dpx0, dpy=dpy0)
tw = line.twiss(method='6d', start="qf1", end="simple_seq$end", init=tw_init)

nemitt_x = exn
nemitt_y = eyn
x_norm, px_norm = xp.generate_2D_gaussian(num_part)
y_norm, py_norm = xp.generate_2D_gaussian(num_part)


def custom_uniformity_score(matrix, x, y, rect_l):
    uniformity_penalty = np.std(matrix)  # Lower is better
    out_of_bounds_penalty = np.sum((x < -rect_l) | (x > rect_l) | (y < -rect_l) | (y > rect_l))
    emptiness_penalty = np.sum(matrix == 0)
    
    # Combine the penalties; adjust weights as necessary
    uniformity_weight = 1.0
    out_of_bounds_weight = 0.01  # Adjust this weight to increase/decrease the penalty for out-of-bounds points
    emptiness_weight = 100.0
    
    # Calculate total score
    score = (uniformity_penalty * uniformity_weight) + \
            (out_of_bounds_penalty * out_of_bounds_weight) + \
            (emptiness_penalty * emptiness_weight)
    return score

def kl_divergence(n_divisions, num_part, counts_matrix):

        number_of_bins = n_divisions**2
        target_per_bin = num_part/number_of_bins # I divide the total number of particles by the number of bins
        P = np.ones((n_divisions, n_divisions))*target_per_bin # I create a square matrix with the correct number of bins
        P = P/P.sum()

        Q = counts_matrix.astype(float)/num_part # This is the matrix where I counted how many particles fall in the bins. The total number of particles inside this region is smaller than num_part.
        Q = np.where(Q==0, np.finfo(float).eps, Q) # Replace any zeros in matrix Q with the smallest positive float number to prevent division by zero errors

        D_KL = np.sum(P * np.log(P / Q)) # Calculate the KL divergence
    
        return D_KL

import scipy.optimize as optimize
import pybobyqa

fetch_rows = lambda indices, attribute: np.array([getattr(twiss.row(index), attribute) for index in indices])

iter = 0
def optimise(params):
    global iter
    print(f"Iteration = {iter}")
    iter+=1
    kQF1, kQD2, kQF3, kQD4, kQ1, kQ2, koct1, koct2 = params
    print(params)

    p, exn, eyn, betx0, bety0, alfx0, alfy0, dx0, dy0, dpx0, dpy0, ex, ey, sige = calc_initial_condition(0.5)

    # Change and track
    line['qf1'].k1 = kQF1
    line['qd2'].k1 = kQD2
    line['qf3'].k1 = kQF3
    line['qd4'].k1 = kQD4
    line['q1'].k1 = kQ1
    line['q2'].k1 = kQ2
    line['oct1'].knl = [0,0,0,koct1]
    line['oct2'].knl = [0,0,0,koct2]

    # Loop through the tracking
    num_part = 2*5000
    x_norm, px_norm = xp.generate_2D_gaussian(num_part)
    y_norm, py_norm = xp.generate_2D_gaussian(num_part)
    part = line.build_particles(x_norm=x_norm, px_norm=px_norm, y_norm=y_norm, py_norm=py_norm,
                                W_matrix=tw.W_matrix[0], particle_on_co=line.particle_ref,
                                nemitt_x=nemitt_x,nemitt_y=nemitt_y)
    # part._init_random_number_generator()
    line.track(part, turn_by_turn_monitor="ONE_TURN_EBE")
    # line_df = line.to_pandas()

    target_s = 100
    s_array = line.record_last_track.s[0]
    differences = np.abs(s_array - target_s)
    s_index = np.argmin(differences)
    x = line.record_last_track.x[:,s_index]
    y = line.record_last_track.y[:,s_index]

    rect_l = 0.05

    # Number of divisions per side
    n_divisions = 20
    side_length = 2* rect_l / n_divisions

    # Initialize an empty list to hold the count of points in each small square
    counts = []
    # Loop over each small square to calculate its bounds and count the points within
    for i in range(n_divisions):
        for j in range(n_divisions):
            # Calculate the bounds of the small square
            x_min = i * side_length - rect_l
            x_max = (i + 1) * side_length - rect_l
            y_min = j * side_length - rect_l
            y_max = (j + 1) * side_length - rect_l
            
            # Count how many points fall within these bounds
            count = np.sum((x >= x_min) & (x < x_max) & (y >= y_min) & (y < y_max))
            counts.append(count)

    # Reshape the counts to match the grid layout for easier interpretation
    counts_matrix = np.reshape(counts, (n_divisions, n_divisions))

    # objective =  custom_uniformity_score(counts_matrix, x, y, rect_l)
    objective = kl_divergence(n_divisions, num_part, counts_matrix)
    
    print (f"OJECTIVE = {objective}")
    return objective

initial_guess = np.array([initial_quad_strength[0], initial_quad_strength[1], initial_quad_strength[2], initial_quad_strength[3], -0.0, 0.0, 0, 0])

lower = np.array([initial_quad_strength[0] - initial_quad_strength[0]*0.5,
                  initial_quad_strength[1] - initial_quad_strength[1]*0.5,
                  initial_quad_strength[2] - initial_quad_strength[2]*0.5,
                  initial_quad_strength[3] - initial_quad_strength[3]*0.5,
                  -0.5,
                  -0.5,
                  0,
                  0])
upper = np.array([initial_quad_strength[0] + initial_quad_strength[0]*0.5,
                  initial_quad_strength[1] + initial_quad_strength[1]*0.5,
                  initial_quad_strength[2] + initial_quad_strength[2]*0.5,
                  initial_quad_strength[3] + initial_quad_strength[3]*0.5,
                  0.5,
                  0.5,
                  400,
                  400])

soln = pybobyqa.solve(optimise, initial_guess, bounds=(lower,upper), rhobeg = 0.5, rhoend=0.0001, scaling_within_bounds=True, seek_global_minimum=True, objfun_has_noise=True, maxfun=10000)
print(soln)

print(soln.x)

# Change and track
line['qf1'].k1 = soln.x[0]
line['qd2'].k1 = soln.x[1]
line['qf3'].k1 = soln.x[2]
line['qd4'].k1 = soln.x[3]
line['q1'].k1 = soln.x[4]
line['q2'].k1 = soln.x[5]
line['oct1'].knl = [0,0,0,soln.x[6]]
line['oct2'].knl = [0,0,0,soln.x[7]]

# Loop through the tracking
num_part = 2*5000
x_norm, px_norm = xp.generate_2D_gaussian(num_part)
y_norm, py_norm = xp.generate_2D_gaussian(num_part)
part = line.build_particles(x_norm=x_norm, px_norm=px_norm, y_norm=y_norm, py_norm=py_norm,
                            W_matrix=tw.W_matrix[0], particle_on_co=line.particle_ref,
                            nemitt_x=nemitt_x,nemitt_y=nemitt_y)
# part._init_random_number_generator()
line.track(part, turn_by_turn_monitor="ONE_TURN_EBE")
line_df = line.to_pandas()



fig, ax = plt.subplots(1,3, figsize=(15,5), tight_layout=True)

target_s = 100
s_array = line.record_last_track.s[0]
differences = np.abs(s_array - target_s)
s_index = np.argmin(differences)
x = line.record_last_track.x[:,s_index]
y = line.record_last_track.y[:,s_index]

rect_l = 0.05
rect = patches.Rectangle((0.0-rect_l, 0.0-rect_l), 2*rect_l, 2*rect_l, linewidth=1, edgecolor='r', facecolor='none')
ax[0].add_patch(rect)

xy = np.vstack([x,y])
z = gaussian_kde(xy)(xy)
# ax[0].scatter(x, y, c=z, s=1, cmap="jet")
ax[0].scatter(x, y, s=1, cmap="jet")

# Number of divisions per side
n_divisions = 20
side_length = 2 * rect_l / n_divisions

# Loop to create and add small square patches
for i in range(n_divisions):
    for j in range(n_divisions):
        small_rect = patches.Rectangle((i * side_length - rect_l, j * side_length - rect_l), side_length, side_length, linewidth=0.5, edgecolor='k', facecolor='none')
        ax[0].add_patch(small_rect)

# Initialize an empty list to hold the count of points in each small square
counts = []

# Loop over each small square to calculate its bounds and count the points within
for i in range(n_divisions):
    for j in range(n_divisions):
        # Calculate the bounds of the small square
        x_min = i * side_length - rect_l
        x_max = (i + 1) * side_length - rect_l
        y_min = j * side_length - rect_l
        y_max = (j + 1) * side_length - rect_l
        
        # Count how many points fall within these bounds
        count = np.sum((x >= x_min) & (x < x_max) & (y >= y_min) & (y < y_max))
        counts.append(count)

# Reshape the counts to match the grid layout for easier interpretation
counts_matrix = np.reshape(counts, (n_divisions, n_divisions))

ax[0].set_xlabel("x")
ax[0].set_ylabel("y")
ax[0].set_title(f"s = {s_array[s_index]} m")
ax[0].set_ylim(-0.1, 0.1)
ax[0].set_xlim(-0.1, 0.1)
ax[0].set_aspect("equal")

objective =  custom_uniformity_score(counts_matrix, x, y, rect_l)
objective_kl = kl_divergence(n_divisions, num_part, counts_matrix)
ax[0].set_title(f"Objective = {objective:.2f}\nObjective KL = {objective_kl:2f}")

ax[1].hist(x,bins=n_divisions, range=(-rect_l,rect_l), color="b");
ax[2].hist(y,bins=n_divisions, range=(-rect_l,rect_l), color="r");

ax[1].axvline(-rect_l)
ax[1].axvline(rect_l)
ax[2].axvline(-rect_l)
ax[2].axvline(rect_l)


plt.savefig("end_of_line.png", facecolor='white', transparent=False, dpi = 300, bbox_inches='tight')