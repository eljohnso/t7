#!/bin/bash

# Define variables
SUB_FILE="script.sub"
SH_FILE="script.sh"
TXT_FILE="script.txt"
PY_FILE="optimise_lattice_octupoles.py"
LATTICE_FILE="simple_seq.seq"
REQUIREMENTS_FILE="requirements.txt"
DESTINATION_PATH="/afs/cern.ch/work/e/eljohnso/public/HTCondor"
LXPLUS_USER="eljohnso@lxplus.cern.ch"

# Transfer the job file to lxplus
scp $SUB_FILE $LXPLUS_USER:$DESTINATION_PATH
scp $SH_FILE $LXPLUS_USER:$DESTINATION_PATH
scp $TXT_FILE $LXPLUS_USER:$DESTINATION_PATH
scp $PY_FILE $LXPLUS_USER:$DESTINATION_PATH
scp $LATTICE_FILE $LXPLUS_USER:$DESTINATION_PATH
scp $REQUIREMENTS_FILE $LXPLUS_USER:$DESTINATION_PATH

# SSH and submit the job
ssh $LXPLUS_USER "cd $DESTINATION_PATH && condor_submit $SUB_FILE"