#!/bin/bash

# Define variables
DESTINATION_PATH="/afs/cern.ch/work/e/eljohnso/public/HTCondor"
LXPLUS_USER="eljohnso@lxplus.cern.ch"

# Transfer the job file to lxplus
scp "$LXPLUS_USER:$DESTINATION_PATH/end_of_line*" ./data
